" Indent automatically depending on filetype
filetype plugin indent on

" Turn on line numbering. Turn it off with "set nonu" 
set number
set title

" Set syntax on
syntax on

" Case insensitive search
set ic

" Higlhight search
set hls

" Wrap text instead of being on one line
set lbr

" Change colorscheme from default to delek
colorscheme delek

" Tabs are 8 characters, and thus indentations are also 8 characters. 
" There are heretic movements that try to make indentations 4 (or even 2!) characters deep, 
" and that is akin to trying to define the value of PI to be 3.
set tabstop=8
set softtabstop=8
set shiftwidth=8
set noexpandtab

" 80 characters line
"set colorcolumn=81
"execute "set colorcolumn=" . join(range(81,335), ',')
"highlight ColorColumn ctermbg=Black ctermfg=DarkRed

" Highlight trailing spaces
" http://vim.wikia.com/wiki/Highlight_unwanted_spaces
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()


set foldmethod=indent
"set foldnestmax=10
set nofoldenable
"set foldlevel=2
